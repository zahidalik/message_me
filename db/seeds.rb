# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(username: "Shazan", password: "shazan5")
User.create(username: "Haura", password: "haura5")
User.create(username: "Muhammad", password: "muhammad5")
User.create(username: "Ali", password: "ali5")
User.create(username: "Shayan", password: "shayan5")
